'use strict';

//Get modal btn
var modalBtn = document.querySelector('.modal-btn');

//Get modal window
var modalWindow = document.getElementById('modal');

//Get close-btn
var closeBtn = document.querySelector('.modal-close');

//Add class open and show modal window
modalBtn.addEventListener('click', function() {
    modalWindow.classList.add('open');
});

//Click on close btn remove class open
closeBtn.addEventListener('click', function() {
    modalWindow.classList.remove('open');
});

//Listen outside click
document.addEventListener('click', function(e) {
    //Click on wrapper
    if (e.target.classList.contains('modal','open') ) {
        modalWindow.classList.remove('open');
    }
});