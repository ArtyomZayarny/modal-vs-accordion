'use strict';

var accordion = document.querySelectorAll('accordion');

//Click on title of accordion
    //span.title
document.addEventListener('click', function(e){
    //Click only on accordion 
    if (e.target.closest('.accordion')) {
        var accordion = e.target.closest('.accordion');
        var item = e.target.closest('.title-item');
        var acc = new Accordion();
        acc.init(accordion,item);
    }
});

function Accordion(parent,item) {
   
function closeAll(accordion) {
    var allSubs = accordion.querySelectorAll('.sub');
    allSubs.forEach(function(item){
     item.classList.remove('open');
    });
}

 function checkSubs(accordion,item) {
     var subsOpen = accordion.querySelectorAll('.sub.open');
     if (subsOpen.length > 1 ) {
         closeAll(accordion);
         showSubs(item);
     } 
     
 }
 function showSubs(item){
     if (!item.querySelector('.sub').classList.contains('open')) {
        item.querySelector('.sub').classList.add('open');
     } else {
        item.querySelector('.sub').classList.remove('open');
     }
 }
 return{
     init: function init (accordion,item) {
         showSubs(item);
         checkSubs(accordion,item);
    }
 }
}


